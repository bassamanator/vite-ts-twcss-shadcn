import { Link } from 'react-router-dom';
import { Button } from '~/components/ui';

export default function NotFoundPage() {
	return (
		<div className='mx-5 flex flex-col items-center justify-center gap-4'>
			<h1 className='xs:text-4xl text-xl font-bold text-primary'>404 - Page not found</h1>
			<p className='text-md xs:text-xl text-center text-primary'>
				The page you're looking for doesn't exist.
			</p>
			<Link to='/' className='no-underline'>
				<Button className=''>Go to Home</Button>
			</Link>
		</div>
	);
}
