const USER_INFO = '_USERINFO';
const TOKEN_KEY = '_JWTTOKEN';

export const authStore = {
	clear(key: string) {
		if (localStorage?.getItem(key)) localStorage.removeItem(key);
		if (sessionStorage?.getItem(key)) sessionStorage.removeItem(key);
		return null;
	},

	clearAppStorage() {
		if (localStorage) localStorage.clear();
		if (sessionStorage) sessionStorage.clear();
	},

	clearToken(tokenKey = TOKEN_KEY) {
		return authStore.clear(tokenKey);
	},

	get(key: string) {
		if (localStorage && localStorage.getItem(key)) {
			const item = localStorage.getItem(key);
			return JSON.parse(item!);
		}

		if (sessionStorage && sessionStorage.getItem(key)) {
			const item = sessionStorage.getItem(key);
			return JSON.parse(item!);
		}

		return null;
	},

	getToken(tokenKey = TOKEN_KEY): string | null {
		return authStore.get(tokenKey);
	},

	getUserInfo(userInfo = USER_INFO): User | null {
		return authStore.get(userInfo);
	},

	set(value: unknown, key: string, saveToLocalStorage: boolean) {
		if (!value) return null;

		if (saveToLocalStorage) {
			return localStorage.setItem(key, JSON.stringify(value));
		}

		if (sessionStorage) {
			return sessionStorage.setItem(key, JSON.stringify(value));
		}

		return null;
	},

	setToken(value = '', saveToLocalStorage = true, tokenKey = TOKEN_KEY) {
		return authStore.set(value, tokenKey, saveToLocalStorage);
	},

	setUserInfo(value?: unknown, saveToLocalStorage = true, userInfoKey = USER_INFO) {
		return authStore.set(value || '', userInfoKey, saveToLocalStorage);
	},
};
