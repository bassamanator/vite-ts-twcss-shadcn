import { type ReactNode, createContext, useEffect, useState } from 'react';
import { authStore } from '~/lib';

type AuthContextType = {
	clearToken: () => void;
	setToken: (value: string) => void;
	token: null | string;
};
export const AuthContext = createContext<AuthContextType>(null!);

export const AuthContextProvider = ({ children }: { children: ReactNode }) => {
	const [token, setToken] = useState<string | null>(null);
	const clearToken = () => setToken(null);

	// Get the token from localStorage when the component mounts
	useEffect(() => {
		const storedToken = authStore.getToken();
		if (storedToken) setToken(storedToken);
	}, []);

	// Add or remove the token
	useEffect(() => {
		if (token) authStore.setToken(token);
		else authStore.clearToken();
	}, [token]);

	return (
		<AuthContext.Provider value={{ token, setToken, clearToken }}>{children}</AuthContext.Provider>
	);
};
