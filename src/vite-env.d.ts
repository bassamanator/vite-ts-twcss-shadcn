/// <reference types="vite/client" />

interface ImportMetaEnv {
	readonly VITE_PUBLIC_API: string; // NOTE
	readonly VITE_PUBLIC_BASEURL: string;
	// more env variables...
}
