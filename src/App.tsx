import { Suspense, lazy, useState } from 'react';
import {
	Route,
	RouterProvider,
	createBrowserRouter,
	createRoutesFromElements,
} from 'react-router-dom';

import reactLogo from './assets/react.svg';
import { RootLayout } from './layouts/RootLayout';
import { ROUTES } from './routes';
import viteLogo from '/vite.svg';

const NotFoundPage = lazy(() => import('./components/NotFoundPage'));

const router = createBrowserRouter(
	createRoutesFromElements(
		<Route path={'/'} element={<RootLayout />}>
			<Route index element={<RootComponent />} />
			<Route path='*' element={<NotFoundPage />} />
		</Route>,
	),
);

function App() {
	return (
		<Suspense
			fallback={<div className='flex h-screen items-center justify-center'>Loading...</div>}
		>
			<RouterProvider router={router} />
		</Suspense>
	);
}

export default App;

function RootComponent() {
	const [count, setCount] = useState(0);

	return (
		<>
			<div>
				<a href='https://vitejs.dev' target='_blank'>
					<img src={viteLogo} className='logo' alt='Vite logo' />
				</a>
				<a href='https://react.dev' target='_blank'>
					<img src={reactLogo} className='logo react' alt='React logo' />
				</a>
			</div>
			<h1>Vite + React</h1>
			<div className='card'>
				<button onClick={() => setCount((count) => count + 1)}>count is {count}</button>
				<p>
					Edit <code>src/App.tsx</code> and save to test HMR
				</p>
			</div>
			<p className='read-the-docs'>Click on the Vite and React logos to learn more</p>
		</>
	);
}
