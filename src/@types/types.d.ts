type User = {
	email: string | null;
	emailVerified?: Date;
	id: string;
};

type PartialBy<T, K extends keyof T> = Omit<T, K> & Partial<Pick<T, K>>;
type Maybe<T> = T | undefined | null;

/**
 * Make a type assembled from several types/utilities cleaner.
 * (easier to read when hovering the type in the IDE).
 */
type CleanType<T> = T extends infer U ? { [K in keyof U]: U[K] } : never;
