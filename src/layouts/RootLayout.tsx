import { Link, Outlet, useLocation } from 'react-router-dom';
import { ModeToggle } from '~/components';
import { ROUTES } from '~/routes';

export const RootLayout = () => {
	const { state }: { state: { fromPath?: string } } = useLocation();

	return (
		<div>
			<div className='h-screen w-full'>
				<HeaderButtons />
				<div className=''>
					<main className='flex h-full w-full items-center justify-center bg-secondary'>
						<Outlet />
					</main>
				</div>
			</div>
		</div>
	);
};

export const HeaderButtons = () => {
	return (
		<div className='flex flex-row items-center gap-1'>
			<ModeToggle className='h-8 w-8 p-0 sm:h-10 sm:w-10' />
		</div>
	);
};
